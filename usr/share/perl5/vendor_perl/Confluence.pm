package Confluence;

$VERSION = 2.1.1;

# Modified for Confluence 4.+ by Frank Pickle
#
# Copyright (c) 2004 Asgeir.Nilsen@telenor.com
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

use RPC::XML;
use RPC::XML::Client;
use Carp;
use strict;
use vars '$AUTOLOAD'; # keep 'use strict' happy

our $API = 'confluence2';

use fields qw(url token client);

# Global variables
our $RaiseError = 0;
our $PrintError = 0;
our $LastError  = '';
our $Debug      = 0;
our $Filter     = 0;

# For debugging
sub _debugPrint {
  require Data::Dumper;

  $Data::Dumper::Terse=1;
  $Data::Dumper::Indent=0;
  $Data::Dumper::Quotekeys=0;

  print STDERR (shift @_);

  while ($_ = shift @_) {
    print STDERR (Data::Dumper::Dumper($_) . (scalar @_ ? ', ' : ''));
  }

  print STDERR "\n";
}

sub setRaiseError {
  shift if ref $_[0];
  carp "setRaiseError expected scalar" unless defined $_[0] and not ref $_[0];
  my $old = $RaiseError;
  $RaiseError = $_[0];
  return $old;
}

sub setPrintError {
  shift if ref $_[0];
  carp "setPrintError expected scalar" unless defined $_[0] and not ref $_[0];
  my $old = $PrintError;
  $PrintError = $_[0];
  return $old;
}
    
sub setDebug {
  shift if ref $_[0];
  carp "setDebug expected scalar" unless defined $_[0] and not ref $_[0];
  my $old = $Debug;
  $Debug = $_[0];
  return $old;
}
    
sub setFilter {
  shift if ref $_[0];
  carp "setFilter expected scalar" unless defined $_[0] and not ref $_[0];
  my $old = $Filter;

  # Keep the default setting unless we're using v2 of the Confluence api
  $Filter = $_[0] if($API eq 'confluence2');
  return $old;
}

sub lastError {
  return $LastError;
}

#  This function converts scalars to RPC::XML strings
sub argcopy {
  my ($arg, $depth) = @_;
  my ($type);

  return $arg if $depth > 1;
  $type = ref($arg);

  if(!$type) {
    if ($arg =~ /^(true|false)$/ and $depth==0) {
      return new RPC::XML::boolean($arg);
    } else {
      return new RPC::XML::string($arg);
    }
  }

  if($type eq "HASH") {
    my %hash;

    foreach my $key (keys %$arg) {
      $hash{$key} = argcopy($arg->{$key}, $depth+1);
    }

    return \%hash;
  }

  if($type eq "ARRAY") {
    my @array = map { argcopy($_, $depth+1) } @$arg;
    return \@array;
  } 

  return $arg;
}

sub new {
  my Confluence $self = shift;
  my ($url, $user, $pass) = @_;
  my ($result);

  $self = fields::new($self) unless(ref($self));

  $self->{url} = shift;

  warn "Creating client connection to $url" if($Debug);

  $self->{client} = new RPC::XML::Client $url;

  warn "Logging in $user" if($Debug);

  $LastError = '';
  $result = $self->{client}->simple_request("$API.login", $user, $pass);

  if(defined($result)) {
    if(ref($result) eq 'HASH' && exists($result->{'faultString'})) {
      $LastError = "REMOTE ERROR: " . $result->{faultString};
    }
  } else {
    $LastError = "XML-RPC ERROR: Unable to connect to " . $self->{url};
  }

  _debugPrint("Result=",$result) if($Debug);

  if($LastError) {
    croak $LastError if $RaiseError;
    warn  $LastError if $PrintError;
  }

  $self->{token} = $LastError ? '' : $result;
  return $LastError ? '' : $self;
}

# login is an alias for new
sub login {
  return new @_;
}

sub updatePage {
  my Confluence $self = shift;
  my ($newPage) = @_;
  my $saveRaise = setRaiseError(0);
  my $result = $self->storePage($newPage);

  setRaiseError($saveRaise);

  if($LastError) {
    if($LastError =~ /already exists/) {
      my $oldPage = $self->getPage($newPage->{space}, $newPage->{title});

      $newPage->{id} = $oldPage->{id};
      $newPage->{version} = $oldPage->{version};
      $result = $self->storePage($newPage);
    } else {
      croak $LastError if $RaiseError;
      warn  $LastError if $PrintError;
    }
  }

  return $LastError ? undef : $result;
}

sub _rpc {
  my Confluence $self = shift;
  my ($method, @parameters) = @_;
  my (@args, $result);

  croak "ERROR: Not connected" unless $self->{token};

  @args = map { argcopy($_, 0) } @parameters;

  _debugPrint("Sending $API.$method ", @args) if($Debug);

  $LastError = '';
  $result = $self->{client}->simple_request("$API.$method", $self->{token}, @args);

  if(defined($result)) {
    if(ref($result) eq 'HASH' && exists($result->{'faultString'})) {
      $LastError = "REMOTE ERROR: " . $result->{faultString};
    }
  } else {
    $LastError = "XML-RPC ERROR: Unable to connect to " . $self->{url};
  }

  _debugPrint("Result=", $result) if($Debug);

  if ($LastError) {
    croak $LastError if $RaiseError;
    warn  $LastError if $PrintError;
  }

  return $LastError ? '' : $result; 
}

# Define commonly used functions to avoid overhead of autoload
sub getPage { 
  my Confluence $self = shift;

  _debugPrint("Parameters=", @_) if($Debug);

  return _rpc($self, 'getPage', @_); 
}
    
sub storePage { 
  my Confluence $self = shift;
  my ($page) = @_;

  _debugPrint("Parameters=", $page) if($Debug);

  $page->{'content'} = '' unless($page->{'content'});
  $page->{'content'} = $self->xlatePage($page->{'content'}) if($Filter);

  return _rpc($self, 'storePage', $page);
}

sub xlatePage {
  my Confluence $self = shift;
  _debugPrint("Parameters=", @_) if($Debug);
  _rpc($self, 'convertWikiToStorageFormat', @_);
}

sub removePage {
  my Confluence $self = shift;
  _debugPrint("Parameters=", @_) if($Debug);
  _rpc($self, 'removePage', @_);
}

sub cleanTrash {
  my Confluence $self = shift;
  _debugPrint("Parameters=", @_) if($Debug);
  _rpc($self, 'purgeFromTrash', @_);
}

# Use autolaod for everything else
sub AUTOLOAD {
  my Confluence $self = shift;

  $AUTOLOAD =~ s/Confluence:://;
  return if $AUTOLOAD =~ /DESTROY/;

  _debugPrint("Parameters=", @_) if($Debug);

  return _rpc($self, $AUTOLOAD, @_);
}

1;
