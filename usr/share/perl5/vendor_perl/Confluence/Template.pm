package Confluence::Template;

# Copyright (C) 2012 Frank Pickle
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use Moose;
use Template;
use Confluence;
use Config::General;
use MIME::Base64;
use Digest::MD5;
use File::Basename;
use Confluence::Script;

# String Properties
has 'TemplateFile' => (
  is => 'rw',
  isa => 'Str',
  default => '',
  trigger => \&_load );

has 'Page'          => ( is => 'ro', isa => 'Str', default => '' );
has 'InlinePerl'    => ( is => 'ro', isa => 'Str', default => '' );
has 'Template'      => ( is => 'ro', isa => 'Str', default => '' );
has 'MD5'           => ( is => 'ro', isa => 'Str', default => '' );

# List Properties
has 'RawConfig'     => ( is => 'ro', isa => 'ArrayRef', default => sub { [] } );
has 'RawInlinePerl' => ( is => 'ro', isa => 'ArrayRef', default => sub { [] } );
has 'RawTemplate'   => ( is => 'ro', isa => 'ArrayRef', default => sub { [] } );
has 'Config'        => ( is => 'ro', isa => 'HashRef', default => sub { {} } );
has 'Map'           => ( is => 'rw', isa => 'HashRef', default => sub { {} } );

# Objects
has 'ConfigObj'     => ( is => 'ro', isa => 'HashRef', default => sub { {} } );
has 'TemplateObj'   => ( is => 'ro', isa => 'HashRef', default => sub { {} } );

# Public Methods
sub process {
  my ($self) = @_;
  my ($template, $page);

  $self->_parseTemplate();
  $template = $self->Template;

  unless($self->TemplateObj->process(\$template, $self->Map, \$page)) {
    &_error($self->TemplateObj->error());
    return 0;
  }

  $self->{'Page'} = $page;
  $self->{'MD5'}  = Digest::MD5->new()->add($self->Page)->hexdigest();
  return 1;
}

sub write {
  my ($self) = @_;
  my ($wiki, $page, $output, $page_id, $result);

  # If nothing has changed in our output, do not write to Confluence...
  return if($self->_checkMD5());

  $wiki = $self->_createConfluenceObject();
  $page = $self->_getConfluencePage($wiki, 0);

  # Remove non-ascii characters...
  $output = $self->Page;
  $output =~ s/[^[:ascii:]]//g;

  if($self->Config->{'REMOVE_BEFORE_UPDATE'}) {
    if($page && exists $page->{'id'}) {
      $page_id = $page->{'id'};
      $wiki->removePage($page_id);
      $page = '';
    }
  }

  if($page) {
    $page->{'content'} = "$output";
    $result = $wiki->updatePage($page);
  } else {
    my $page_args = { space   => $self->Config->{'CONFLUENCE_SPACE'},
                      title   => $self->Config->{'CONFLUENCE_PAGE'},
                      content => "$output" };
    my $parent_id = $self->_checkParent();

    $page_args->{'parentId'} = $parent_id if($parent_id);
    $result = $wiki->updatePage($page_args);
  }

  if($result) {
    $self->_processAttachments($wiki, $result->{'id'});

    # Clean trash...
    $wiki->cleanTrash($self->Config->{'CONFLUENCE_SPACE'}, $page_id) if($page_id);
  }

  $wiki->logout();

  return 1;
}

# Private Methods
sub _load {
  my ($self) = @_;
  my $tracker = '';

  unless( -f $self->TemplateFile ) {
    &_error($self->TemplateFile . ' does not exist');
    return 0;
  }

  unless(open(TEMPLATE, '<' . $self->TemplateFile)) {
    &_error('Cannot open ' . $self->TemplateFile);
    return 0;
  }

  while(my $line = <TEMPLATE>) {
    if($line =~ m/^\[configuration\]\s*$/) {
      $tracker = 'configuration';
    } elsif($line =~ m/^\[inline-perl\]\s*$/) {
      $tracker = 'inline-perl';
    } elsif($line =~ m/^\[template\]\s*$/) {
      $tracker = 'template';
    } elsif($tracker eq 'configuration') {
      push @{$self->{'RawConfig'}}, $line;
    } elsif($tracker eq 'inline-perl') {
      push @{$self->{'RawInlinePerl'}}, $line;
    } elsif($tracker eq 'template') {
      push @{$self->{'RawTemplate'}}, $line;
    }
  }

  $self->_createConfigObj();
  %{$self->{'Config'}} = $self->ConfigObj->getall();

  $self->{'TemplateObj'} = new Template();

  if(@{$self->RawInlinePerl}) {
    $self->{'InlinePerl'}  = join('', @{$self->RawInlinePerl});
  }

  close(TEMPLATE);
  return 1;
}

sub _createConfigObj {
  my ($self) = @_;

  if(-f $ENV{'CPG_CONFIG'}) {
    my $config_obj = new Config::General($ENV{'CPG_CONFIG'});
    my %config = $config_obj->getall();

    $self->{'ConfigObj'} = new Config::General(
      -DefaultConfig => \%config,
      -String => $self->RawConfig,
      -MergeDuplicateOptions => 1,
      -AllowMultiOptions => 1
    );
  } else {
    $self->{'ConfigObj'} = new Config::General( -String => $self->RawConfig );
  }
}

sub _parseTemplate {
  my ($self) = @_;
  my ($tracker, $script);

  foreach my $line (@{$self->RawTemplate}) {
    if($line =~ m/^<\?(\w+)\s*$/) {
      $tracker = $1;
    } elsif($line =~ m/^\?>\s*$/) {
      my $script_obj = new Confluence::Script();

      unless(exists $self->Config->{'interpreters'}->{$tracker}) {
        $tracker = '';
        $script = '';
        next;
      }

      $script_obj->Interpreter($self->Config->{'interpreters'}->{$tracker});

      if($self->Config->{'TMP_DIR'}) {
        $script_obj->ScriptPath($self->Config->{'TMP_DIR'});
      }

      $script_obj->Content($script);
      $self->{'Template'} .= $script_obj->Output;

      $tracker = '';
      $script = '';
    } elsif($tracker) {
      $script .= $line;
    } else {
      $self->{'Template'} .= $line;
    }
  }
}

sub _checkParent {
  my ($self) = @_;
  my ($wiki, $page);

  unless(exists $self->Config->{'CONFLUENCE_PARENT'}) {
    $self->{'Config'}->{'CONFLUENCE_PARENT'} = 'Home';
  }

  $wiki = $self->_createConfluenceObject();
  $page = $self->_getConfluencePage($wiki, 1);

  return 0 unless(ref($page) eq 'HASH');

  unless($page->{'id'}) {
    &_error('Cannot get parent page id');
    return 0;
  }

  return $page->{'id'};
}

sub _createConfluenceObject {
  my ($self) = @_;
  my $obj = {};
  
  if(exists $self->Config->{'API_VERSION'}) {
    $Confluence::API = 'confluence1' if($self->Config->{'API_VERSION'} == 1);
  }

  my $password = $self->Config->{'CONFLUENCE_PASS'};

  if (&_isBase64($password)) {
    $password = decode_base64($password);
    chomp($password);
  }

  $obj = new Confluence( $self->Config->{'RPC_URL'},
                         $self->Config->{'CONFLUENCE_USER'},
                         $password );

  unless($obj) {
    &_error('Failed to create a valid Confluence object');
    return 0;
  }

  $obj->setDebug(1)      if($self->Config->{'RPC_DEBUG'});
  $obj->setPrintError(1) if($self->Config->{'RPC_PRINT_ERROR'});
  $obj->setFilter(1)     if($self->Config->{'MARKUP_FILTER'});

  return $obj;
}

sub _getConfluencePage {
  my ($self, $wiki, $is_parent) = @_;
  my $page_name = $self->Config->{'CONFLUENCE_PAGE'};
  my ($page);

  $page_name = $self->Config->{'CONFLUENCE_PARENT'} if($is_parent);

  eval {
    $page = $wiki->getPage( $self->Config->{'CONFLUENCE_SPACE'},
                            $page_name );
  };

  return $page;
}

sub _processAttachments {
  my ($self, $wiki, $page_id) = @_;

  return 0 unless($page_id);
  return 0 unless(exists $self->Config->{'ATTACHMENT'});

  if(ref($self->Config->{'ATTACHMENT'}) eq 'ARRAY') {
    foreach my $config (@{$self->Config->{'ATTACHMENT'}}) {
      $self->_addAttachment(
        $wiki,
        $page_id,
        $config
      );
    }
  } else {
    $self->_addAttachment(
      $wiki,
      $page_id,
      $self->Config->{'ATTACHMENT'}
    );
  }
}

sub _addAttachment {
  my ($self, $wiki, $page_id, $config) = @_;

  my $file = $config->{'path'};
  my @pass_keys = ( 'fileName', 'comment', 'contentType' );
  my $parameters = {};
  my $data = '';

  unless(-f $file) {
    &_error('Cannot find ' . $file);
    return 0;
  }

  foreach my $key (@pass_keys) {
    if(defined $config->{$key}) {
      $parameters->{$key} = $config->{$key};
    }
  }

  unless(open(FILE, '<' . $file)) {
    &_error('Cannot open ' . $file);
    return 0;
  }

  binmode(FILE);
  $data .= $_ while(<FILE>);

  $data = new RPC::XML::base64($data) if(-T FILE);

  close(FILE);

  return $wiki->addAttachment(
    $page_id,
    $parameters,
    $data
  );
}

sub _checkMD5 {
  my ($self) = @_;
  my $dir       = dirname($self->TemplateFile);
  my $base_file = basename($self->TemplateFile);
  my $md5_dir   = $dir . '/digests';
  my $md5_file  = $md5_dir . '/' . $base_file . '.md5';
  my $return    = 0;
  my ($fh);

  mkdir($md5_dir, 0700) unless(-d $md5_dir);

  # If the MD5 file exists, check it's contents against the current MD5...
  if(-f $md5_file) {
    if(open($fh, '<', $md5_file)) {
      my $oldMD5 = <$fh>;
      close($fh);

      chomp($oldMD5);
      $return = 1 if($oldMD5 eq $self->MD5);
    } else {
      &_error('Cannot open ' . $md5_file);
    }
  }

  # Save current MD5...
  if(open($fh, '>', $md5_file)) {
    print $fh $self->MD5 . "\n";
    close($fh);
  } else {
    &_error('Cannot open ' . $md5_file);
  }

  return $return;
}

# Private Function
sub _error {
  my ($msg) = @_;
  print STDERR __PACKAGE__ . ":  $msg\n";
}

sub _isBase64 {
  my ($string) = @_;
  return 0 unless($string =~ m/[A-Za-z0-9+\/=]/);
  return 0 unless(length($string) % 4 == 0);
  return 1;
}

no Moose;

1;
