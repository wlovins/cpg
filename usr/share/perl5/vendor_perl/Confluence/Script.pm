package Confluence::Script;

# Copyright (C) 2012 Frank Pickle
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use Moose;

# String Properties
has 'Content' => (
  is => 'rw',
  isa => 'Str',
  default => '',
  trigger => \&_load
);

has 'ScriptFile'  => (
  is => 'ro',
  isa => 'Str',
  default => \&_fileName 
);

has 'Interpreter' => ( is => 'rw', isa => 'Str', default => '' );
has 'ScriptPath'  => ( is => 'rw', isa => 'Str', default => '/tmp' );
has 'Output'      => ( is => 'ro', isa => 'Str', default => '' );

# List Properties
has 'RawOutput'   => ( is => 'ro', isa => 'ArrayRef', default => sub { [] } );

# Private Methods
sub _load {
  my ($self) = @_;
  my $file = $self->ScriptPath . '/' . $self->ScriptFile;
  my $command = $self->Interpreter . ' ' . $file;

  if($self->Content) {
    unless(open(SCRIPT, '>' . $file)) {
      &_error('Cannot open ' . $file . ":  $!");
      return 0;
    }

    print SCRIPT '#!' . $self->Interpreter . "\n";

    print SCRIPT "<?\n" if($self->Interpreter =~ m/php/);
    print SCRIPT $self->Content;
    print SCRIPT "?>\n" if($self->Interpreter =~ m/php/);

    close(SCRIPT);

    @{$self->{'RawOutput'}} = `$command`;

    foreach my $line (@{$self->{'RawOutput'}}) {
      $self->{'Output'} .= $line;
    }

    unlink($file);
  }

  return 1;
}

sub _fileName {
  my ($self) = @_;
  my @chars = ('a' .. 'z', 'A' .. 'Z', 0 .. 9);
  $self->{'ScriptFile'} = (join('', @chars[ map { rand @chars} ( 1 .. 16) ]));
}

# Private Functions
sub _error {
  my ($msg) = @_;
  print STDERR __PACKAGE__ . ":  $msg\n";
}

no Moose;

1;
