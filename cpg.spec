%define ver 1.3.0
%define buildnum 1

Name: cpg
Summary: System to automate creation and updating of Confluence pages
Version: %{ver}
Release: %{buildnum}%{?dist}
License: GPL v3
Group: Applications/System
BuildRoot: %{_tmppath}/build-root-%{name}
BuildArch: noarch
ExclusiveOS: linux
AutoReqProv: no
Requires: perl-Moose
Requires: perl-Template-Toolkit
Requires: perl-Config-General
Requires: perl-RPC-XML
Requires: perl-Crypt-SSLeay
Source0: %{name}.tar.gz

%description
CPG (Confluence Page Generator) is a system that will automatically
create and update Confluence pages through cron jobs utilizing perl
and the Template Toolkit.

%pre
/usr/bin/getent group %{name} >/dev/null || /usr/sbin/groupadd -r %{name}
/usr/bin/getent passwd %{name} >/dev/null || /usr/sbin/useradd -s /bin/bash -g %{name} -c 'CPG user' -d %{_var}/lib/%{name} %{name} 2> /dev/null

%prep
%setup -q

%build

%install
%{__rm} -rf %{buildroot}

%{__mkdir_p} -m 0755 $RPM_BUILD_ROOT
%{__mkdir_p} -m 0755 $RPM_BUILD_ROOT%{_sysconfdir}/cpg.d
%{__mkdir_p} -m 0755 $RPM_BUILD_ROOT%{_var}/lib/%{name}

%{__cp} -ap $RPM_BUILD_DIR/$RPM_PACKAGE_NAME-$RPM_PACKAGE_VERSION/* $RPM_BUILD_ROOT
%{__mv} $RPM_BUILD_ROOT/LICENSE $RPM_BUILD_ROOT%{_var}/lib/%{name}
%{__mv} $RPM_BUILD_ROOT/README $RPM_BUILD_ROOT%{_var}/lib/%{name}

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files
%attr(0600,%{name},%{name}) %config %{_sysconfdir}/cpg.conf
%attr(0755,%{name},%{name}) %{_sysconfdir}/cpg.d
%attr(0644,%{name},%{name}) %{_sysconfdir}/profile.d/cpg.sh
%attr(0750,%{name},%{name}) %{_bindir}/cpg
%defattr(0444,root,root,0755)
%{perl_vendorlib}
%{_var}/lib/%{name}/LICENSE
%{_var}/lib/%{name}/README
%attr(0700,root,root) %{_usr}/libexec/cpg_cron_loader
%attr(0644,root,root) %{_sysconfdir}/cron.d/cpg
%exclude /cpg.spec

# TODO: convert readme into a man page
#       should home dir be /usr/share/cpg?

%post

%preun

%postun

%changelog
* Tue Jul 10 2012 Frank Pickle <frank.pickle@icainformatics.com> - 1.3.0-1
Initial Bit Bucket build
